//This is a single line comment
		/*
		This is a multiline
		comment
		shorcut key: ctrl + shift +/(windows)
					 cmd + shift / (mac) or shift option a
		*/

		//alert("Hello World");
		//console.log("Hello World");
		//console.log(3+2);
		//let name = 'Zuitt'

		let name;
		name = "Zuitt";

		let age = 22;

		console.log(name);

		const boilingPoint = 100;
		//boilingPoint = 200; error

		console.log(boilingPoint);
		console.log (age+'3'); //22
		console.log(name+ 'hello') //Zuitthello

		//Boolean
		let isAlive = 'true';
		console.log(isAlive);

		//Arrays
		let grades = [98, 96, 95, 90];

		console.log(grades); //grade[0]
		let movie = "jaws";
		//console.log(song); //not defined
		let isp;
		console.log(isp);

		//Undefined - represents the state of a variable that has been declared but without an assigned value

		//null - used intentionally to express the absence of a value in a declared/ initialized variable

		let spouse = null
		console.log(spouse);

		//objects - special kind of data type is used to mimic real word object/ item

		/*
		Syntax:
			let objectName = { 
			property: keyValue, 
			property2: keyValue2
			property3: keyValue3

			}
		*/

		let myGrades = {
			firstGrading: 98,
			secondGrading: 92,
			thirdGrading: 90,
			fourthGrading: 94.6
		}	

		console.log(myGrades);

		let person = {
			fullName: 'Alonzo Cruz',
			age: 25,
			isMarried: false,
			conatact: ['123', '456'],
			address: {
				houseNumber: 345,
				city: 'Manila'
			}
		}
			console.log(person);